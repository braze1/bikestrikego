package com.braze.bikestrikego.repository

import android.content.ContentValues.TAG
import android.content.Context
import android.location.Location
import android.util.Log
import com.braze.bikestrikego.cache.BikeStrikeGoCache
import com.braze.bikestrikego.model.BikeStrikeGo
import com.braze.bikestrikego.util.DataState
import com.braze.bikestrikego.util.Utils
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import java.lang.Exception
import javax.inject.Inject

class MainRepository
@Inject
constructor(
//    private val dao: BikeStrikeGoDao,
    private val context: Context,
//    private val cache: BikeStrikeGoCache =  BikeStrikeGoCache.instance
//    private val retrofit: Retrofit,
//    private val cacheMapper: CacheMapper,
//    private val networkMapper: SensorMapper
) {

    suspend fun getBSGOdata(location: Location): Flow<DataState<BikeStrikeGo>> = flow {
        val cache: BikeStrikeGoCache = BikeStrikeGoCache.instance
//        emit(DataState.Loading)
        // just for test
//        delay(2000)

        try {
            //get last distance chunk data
            val chunkDistance = if (cache.containsKey("currentLong")) {
                Utils.getDistance(
                    cache["currentLat"] as Double,
                    cache["currentLong"] as Double,
                    location.latitude,
                    location.longitude
                )
            } else 0.0

            val distance: Double = if (cache.containsKey("distance")) {
                chunkDistance + cache["distance"] as Double
            } else chunkDistance

            //fill up the cache
            if (cache.containsKey("initLong")) {
                cache["currentLong"] = location.longitude
                cache["currentLat"] = location.latitude
                cache["distance"] = distance
                cache["altitude"] = location.altitude
            } else {
                cache["initLong"] = location.longitude
                cache["initLat"] = location.latitude
                cache["distance"] = 0.0
                cache["altitude"] = location.altitude
                delay(3000) //wait for the first GPS calibration
            }

            val bikeStrikeGo = BikeStrikeGo(
                0,
                (location.speed * 3.6).toInt(),
                location.latitude,
                location.longitude,
                location.altitude,
                location.time,
                distance
            )


//            dao.insert(cacheMapper.mapToEntity(bikeStrikeGo))
//            val cachedBikeStrikeGo = dao.get()
//            emit(DataState.Success(cacheMapper.mapFromEntity(cachedBikeStrikeGo)))
            emit(DataState.Success(bikeStrikeGo))
        } catch (e: Exception) {
            emit(DataState.Error(e))
        }
    }


    suspend fun stopBSGOdata(): Flow<DataState<BikeStrikeGo>> = flow {
        val cache: BikeStrikeGoCache = BikeStrikeGoCache.instance
        Log.d(TAG, "EVENT stopBSGOdata: STOP")
        emit(DataState.Loading)
        delay(3000)
        //get last location from the DB

        val altitude = if (cache.containsKey("altitude")) {
            cache["altitude"] as Double
        } else {
            0.0
        }

        val distance = if (cache.containsKey("distance")) {
            cache["distance"] as Double
        } else {
            0.0
        }

        try {
            val bikeStrikeGo = BikeStrikeGo(
                id = 0,
                speed = 0,
                latitude = 0.0,
                longitude = 0.0,
                altitude = altitude,
                time = 0L,
                distance = distance,
            )
            cache.clear()
            // do math and statistics
//            dao.insert(cacheMapper.mapToEntity(bikeStrikeGo))
//            val cachedBikeStrikeGo = dao.get()
//            emit(DataState.Success(cacheMapper.mapFromEntity(cachedBikeStrikeGo)))
            emit(DataState.Success(bikeStrikeGo))
        } catch (e: Exception) {
            emit(DataState.Error(e))
        }
    }

}