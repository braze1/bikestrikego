package com.braze.bikestrikego.room

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.braze.bikestrikego.room.BikeStrikeGoCacheEntity

@Dao
interface BikeStrikeGoDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(BikeStrikeGoEntity: BikeStrikeGoCacheEntity): Long

    @Query("SELECT * FROM bike_strike_go")
    suspend fun get(): BikeStrikeGoCacheEntity
}