package com.braze.bikestrikego.room

import com.braze.bikestrikego.model.BikeStrikeGo
import com.braze.bikestrikego.util.EntityMapper
import com.braze.bikestrikego.room.BikeStrikeGoCacheEntity
import javax.inject.Inject

class CacheMapper
@Inject
constructor():
    EntityMapper<BikeStrikeGoCacheEntity, BikeStrikeGo> {

    override fun mapFromEntity(entity: BikeStrikeGoCacheEntity): BikeStrikeGo {
        return BikeStrikeGo(
            id = entity.id,
            speed = entity.speed,
            latitude = entity.latitude,
            longitude = entity.longitude,
            altitude = entity.altitude,
            time = entity.time,
            distance = entity.distance,
        )
    }

    override fun mapToEntity(domainModel: BikeStrikeGo): BikeStrikeGoCacheEntity {
        return BikeStrikeGoCacheEntity(
            id = domainModel.id,
            speed = domainModel.speed,
            latitude = domainModel.latitude,
            longitude = domainModel.longitude,
            altitude = domainModel.altitude,
            time = domainModel.time,
            distance = domainModel.distance
        )
    }

    fun mapFromEntityList(entities: List<BikeStrikeGoCacheEntity>): List<BikeStrikeGo>{
        return entities.map { mapFromEntity(it) }
    }
}











