package com.braze.bikestrikego.room

import androidx.room.Database
import androidx.room.RoomDatabase

@Database(entities = [BikeStrikeGoCacheEntity::class ], version = 1)
abstract class BikeStrikeGoDatabase: RoomDatabase() {

    abstract fun bikeStrikeGoDao(): BikeStrikeGoDao

    companion object{
        val DATABASE_NAME: String = "bike_strike_go_db"
    }


}