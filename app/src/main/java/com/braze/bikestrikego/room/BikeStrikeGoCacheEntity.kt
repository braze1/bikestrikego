package com.braze.bikestrikego.room

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "bike_strike_go")
class BikeStrikeGoCacheEntity(

    @PrimaryKey(autoGenerate = false)
    @ColumnInfo(name = "id")
    var id: Int,

    @ColumnInfo(name = "speed")
    var speed: Int,

    @ColumnInfo(name = "latitude")
    var latitude: Double,

    @ColumnInfo(name = "longitude")
    var longitude: Double,

    @ColumnInfo(name = "altitude")
    var altitude: Double,

    @ColumnInfo(name = "time")
    var time: Long,

    @ColumnInfo(name = "distance")
    var distance: Double,
)



