package com.braze.bikestrikego.model


data class BikeStrikeGo(
    var id: Int,
    var speed: Int,
    var latitude: Double,
    var longitude: Double,
    var altitude: Double,
    var time: Long,
    var distance: Double,
//    var speedAccuracyMetersPerSecond: Float,
//    var distance: Int,
//    var averageSpeed: Int,
//    var maxSpeed: Int,
//    var calories: Int,
//    var altitude: Int,

)
