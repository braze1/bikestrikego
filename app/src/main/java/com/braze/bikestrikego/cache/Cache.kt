package com.braze.bikestrikego.cache

interface Cache {
    val size: Int

    operator fun set(key: Any, value: Any)

    operator fun get(key: Any): Any?

    fun remove(key: Any): Any?

    fun clear()

    fun containsKey(key: Any): Boolean
}