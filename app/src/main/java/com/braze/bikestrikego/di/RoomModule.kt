package com.braze.bikestrikego.di

import android.content.Context
import androidx.room.Room
import com.braze.bikestrikego.room.BikeStrikeGoDao
import com.braze.bikestrikego.room.BikeStrikeGoDatabase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
object RoomModule {

    @Singleton
    @Provides
    fun provideBlogDb(@ApplicationContext context: Context): BikeStrikeGoDatabase {
        return Room
            .databaseBuilder(
                context,
                BikeStrikeGoDatabase::class.java,
                BikeStrikeGoDatabase.DATABASE_NAME)
            .fallbackToDestructiveMigration()
            .build()
    }

    @Singleton
    @Provides
    fun provideBikeStrikeGoDAO(bikeStrikeGoDatabase: BikeStrikeGoDatabase): BikeStrikeGoDao {
        return bikeStrikeGoDatabase.bikeStrikeGoDao()
    }
}