package com.braze.bikestrikego.di

import com.braze.bikestrikego.repository.MainRepository
import com.braze.bikestrikego.room.BikeStrikeGoDao
import com.braze.bikestrikego.room.CacheMapper
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object RepositoryModule {

//    @Singleton
//    @Provides
//    fun provideMainRepository(
//        bikeStrikeGoDao: BikeStrikeGoDao,
//        cacheMapper: CacheMapper,
//    ): MainRepository {
//        return MainRepository(bikeStrikeGoDao, cacheMapper)
//    }
}














