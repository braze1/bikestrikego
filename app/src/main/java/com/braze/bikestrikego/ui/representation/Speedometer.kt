package com.braze.bikestrikego.ui.representation

import androidx.compose.foundation.Canvas
import androidx.compose.foundation.layout.*
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.graphics.*
import androidx.compose.ui.graphics.drawscope.DrawStyle
import androidx.compose.ui.graphics.drawscope.Stroke
import androidx.compose.ui.graphics.drawscope.drawIntoCanvas
import com.braze.bikestrikego.R

@Composable
fun Speedometer(
    progress: Int
) {
    //275
    val arcDegrees = 300
    //135f
    val startArcAngle = 120f
    //-45
    val startStepAngle = -60
    //55 ok
    val numberOfMarkers = 60
    val degreesMarkerStep = arcDegrees / numberOfMarkers // 5 deg for the next mark
    val currentSpeed = if (progress > 60) 60 else progress
    Canvas(
        modifier = Modifier
            .fillMaxWidth()
            .aspectRatio(1f),
        onDraw = {
            drawIntoCanvas { canvas ->
                val width = drawContext.size.width
                val height = drawContext.size.height
                val shift = 2
                // center dot offset
                val centerOffset = Offset(width / 2f, height / 2f)
                val upLeftOffset = Offset((width / 2f) - shift, (height / 2f) - shift)
                val downRightOffset = Offset((width / 2f) + shift, (height / 2f) + shift)
                // 3/4 circle offset
                val quarterOffset = Offset(width / 4f, height / 4f)

                //colorize dash
                val (mainColor, secondaryColor) = when {
                    progress > 0 -> Color(0xFF9CCEE1) to Color(0xFF9CCEE1)
//                    progress < 20 -> Color(0xFFD32F2F) to Color(0xFFFFCDD2)
//                    progress < 40 -> Color(0xFFF57C00) to Color(0xFFFFE0B2)
                    else -> Color(0xFF9CCEE1) to Color(0xFF9CCEE1)
                }
                val paint = Paint().apply {
                    color = mainColor
                }
                val centerArcSize = Size(width / 2f, height / 2f)
                val centerArcStroke = Stroke(20f, 0f, StrokeCap.Round)

                val textPaint = android.graphics.Paint()
                textPaint.textSize = 64f
                textPaint.color = 0xFF464646.toInt() //0xFF464646

                drawCircle(Color(0xFF464646),(width / 2f) - 4f , downRightOffset, 1f, style = Stroke(4f) )
                drawCircle(Color(0xFF9CCEE1),(width / 2f ) - 4f, centerOffset)
                drawCircle(Color(0xFF464646),(width / 2f) - 28f , upLeftOffset, 1f, style = Stroke(4f) )
                drawCircle(Color.White,(width / 2f) - 28f, centerOffset)

                //middle circle
                drawArc(
                    secondaryColor,
                    startArcAngle,
                    arcDegrees.toFloat(),
                    false,
                    topLeft = quarterOffset,
                    size = centerArcSize,
                    style = centerArcStroke
                )
                // middle arc colorize
                drawArc(
                    mainColor,
                    startArcAngle,
                    (degreesMarkerStep * currentSpeed).toFloat(),
                    false,
                    topLeft = quarterOffset,
                    size = centerArcSize,
                    style = centerArcStroke
                )

                //center of screen 3 circles
                drawCircle(Color(0xFF9CCEE1),50f, centerOffset)
                drawCircle(Color.White, 25f, centerOffset)
                drawCircle(Color(0xFF464646), 20f, centerOffset)

                //draw ticks
                for ((counter, degrees) in (startStepAngle..(startStepAngle + arcDegrees) step degreesMarkerStep).withIndex()) {
                    val lineEndX = 80f
                    paint.color = mainColor

                    val lineStartX = if (counter % 5 == 0) {
                        // main pins width for the outer circle
                        paint.strokeWidth = 8f
                            1f
                    } else {
                        paint.strokeWidth = 4f
                        lineEndX * .4f  //height of secondary pins
                    }
                    canvas.save()
                    canvas.rotate(degrees.toFloat(), width / 2f, height / 2f)
                    canvas.drawLine(
                        Offset(lineStartX + 32, height  / 2f),
                        Offset(lineEndX + 28, height / 2f),
                        paint
                    )

                    // draw pin
                    if (counter == currentSpeed) {
                        paint.color = Color(0xFF464646)
                        canvas.drawPath(
                            Path().apply {
                                moveTo(width / 2, (height / 2) - 5)
                                lineTo(width / 2, (height / 2) + 5)
                                lineTo(width / 8f, (height / 2) + 5)
                                lineTo(width / 16f, (height / 2))
                                lineTo(width / 8f, (height / 2) - 5)
                                lineTo(width / 2, (height / 2) - 5)
                                close()
                            },
                            paint
                        )
                    }

                    //draw numbers
                    if (counter % 10 == 0) {
                        drawIntoCanvas {
                            val x = lineStartX * 130
                            val index = width * 6.8 / 1080
                            val y = lineEndX * index.toFloat()
                            canvas.rotate(-degrees.toFloat(), x + 40, y - 10)
                            it.nativeCanvas.drawText(counter.toString(), x, y, textPaint)
                        }
                    }
                    canvas.restore()
                }
            }
        }
    )
}
