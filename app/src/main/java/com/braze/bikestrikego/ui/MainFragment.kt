package com.braze.bikestrikego.ui

import android.Manifest
import android.content.*
import android.content.ContentValues.TAG
import android.content.SharedPreferences.OnSharedPreferenceChangeListener
import android.content.pm.PackageManager
import android.location.Location
import android.net.Uri
import android.os.Bundle
import android.os.IBinder
import android.os.SystemClock
import android.provider.Settings
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Chronometer
import android.widget.ProgressBar
import android.widget.TextView
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.animation.core.Animatable
import androidx.compose.animation.core.FastOutLinearInEasing
import androidx.compose.animation.core.tween
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.layout.*
import androidx.compose.material.Card
import androidx.compose.material.Text
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.app.ActivityCompat
import androidx.core.content.PermissionChecker.checkSelfPermission
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.braze.bikestrikego.BuildConfig
import com.braze.bikestrikego.R
import com.braze.bikestrikego.model.BikeStrikeGo
import com.braze.bikestrikego.service.ForegroundLocationService
import com.braze.bikestrikego.ui.representation.Speedometer
import com.braze.bikestrikego.util.DataState
import com.braze.bikestrikego.util.SharedPreferenceUtil
import com.google.android.material.snackbar.Snackbar
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlin.math.round


private const val REQUEST_FOREGROUND_ONLY_PERMISSIONS_REQUEST_CODE = 34

@ExperimentalCoroutinesApi
@AndroidEntryPoint
class MainFragment
constructor(
//    private val someString: String
) : Fragment() {

    private lateinit var mainFragmentContainer: ConstraintLayout
    private lateinit var progressbar: ProgressBar
    private lateinit var timer: Chronometer
    private lateinit var distance: TextView
    private lateinit var altitude: TextView
    private val speed: MutableState<Int> = mutableStateOf(0)
    private val isChronometerRunning: MutableState<Boolean> = mutableStateOf(false)
    private var previousSpeed: Int = 0
    private var pauseOffset: Long = 0
    private val viewModel: MainViewModel by viewModels()

    private var foregroundLocationServiceBound = false
    private var foregroundPermissionApproved = false

    // Provides location updates for while-in-use feature.
    private var foregroundLocationService: ForegroundLocationService? = null

    // Listens for location broadcasts from ForegroundOnlyLocationService.
    private lateinit var foregroundBroadcastReceiver: ForegroundBroadcastReceiver
    private lateinit var sharedPreferences: SharedPreferences
    private lateinit var listener: OnSharedPreferenceChangeListener
    private lateinit var foregroundLocationButton: Button

    // Monitors connection to the while-in-use service.
    private val foregroundServiceConnection = object : ServiceConnection {

        override fun onServiceConnected(name: ComponentName, service: IBinder) {
            val binder = service as ForegroundLocationService.LocalBinder
            foregroundLocationService = binder.service
            foregroundLocationServiceBound = true
        }

        override fun onServiceDisconnected(name: ComponentName) {
            foregroundLocationService = null
            foregroundLocationServiceBound = false
        }
    }

    private val provideRationale = registerForActivityResult(
        ActivityResultContracts.RequestPermission()
    ) { isGranted ->
        if (isGranted) {
            foregroundPermissionApproved = true
            // If the user denied a previous request, but didn't check "Don't ask again", provide
            // additional rationale.
            Snackbar.make(
                mainFragmentContainer,
                R.string.permission_rationale,
                Snackbar.LENGTH_LONG
            ).setAction(android.R.string.ok) {
                activity?.let { it1 ->
                    ActivityCompat.requestPermissions(
                        it1,
                        arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                        REQUEST_FOREGROUND_ONLY_PERMISSIONS_REQUEST_CODE
                    )
                }
            }
        } else {
            foregroundPermissionApproved = false
            Log.d(TAG, "Request foreground only permission")
            requestPermissions(
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                REQUEST_FOREGROUND_ONLY_PERMISSIONS_REQUEST_CODE
            )
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        subscribeObservers()
        progressbar = view.findViewById(R.id.progress_bar)
        timer = view.findViewById(R.id.time_value)
        distance = view.findViewById(R.id.distance_value)
        altitude = view.findViewById(R.id.altitude_value)
        mainFragmentContainer = view.findViewById(R.id.container)
        foregroundBroadcastReceiver = ForegroundBroadcastReceiver()

        sharedPreferences = activity!!.getSharedPreferences(
            getString(R.string.preference_file_key),
            Context.MODE_PRIVATE
        )

        listener =
            OnSharedPreferenceChangeListener { prefs, key -> onSharedPreferenceChanged(prefs, key) }
        foregroundLocationButton = view.findViewById(R.id.start_stop)
        foregroundLocationButton.setOnClickListener {
            val enabled = sharedPreferences.getBoolean(
                SharedPreferenceUtil.KEY_FOREGROUND_ENABLED, false
            )
            if (enabled) {
                foregroundLocationService?.unsubscribeToLocationUpdates()
                viewModel.setStateEvent(MainStateEvent.SetStopEvent)
                pauseChronometer()

            } else {
                if (fineLocationPermissionApproved()) {
                    foregroundLocationService?.subscribeToLocationUpdates()
                    resetChronometer()
                    startChronometer()
                        ?: Log.d(TAG, "Service Not Bound")
                } else {
                    requestForegroundPermissions()
                }
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for main fragment
        val fragmentView = inflater.inflate(R.layout.fragment_main, container, false)
            .apply {
                findViewById<ComposeView>(R.id.speedometer).setContent {
                    val progress =
                        remember(speed.value) { Animatable(initialValue = previousSpeed.toFloat()) }
                    val scope = rememberCoroutineScope()
                    scope.launch {
                        progress.animateTo(
                            targetValue = speed.value.toFloat(),
                            animationSpec = tween(
                                durationMillis = 60,
                                easing = FastOutLinearInEasing
                            )
                        )
                        delay(50)
                    }
                    BoxWithConstraints(
                        modifier = Modifier.wrapContentSize(),
                        contentAlignment = Alignment.Center
                    ) {
                        val speedBoxSize = maxWidth * 80 / 380
                        val fontBoxSize = (95 * speedBoxSize.value / 174)
                        Speedometer(progress = round(progress.value).toInt())

                        Card(
                            border = BorderStroke(2.dp, Color(0xFF9CCEE1)),
                            modifier = Modifier
                                .align(Alignment.BottomCenter)
                                .width(speedBoxSize)
                                .height(speedBoxSize + 22.dp)
                                .padding(bottom = 22.dp)
                        ){
                            Text(
                                round(progress.value).toInt().toString(),
                                textAlign = TextAlign.Center,
                                modifier = Modifier.wrapContentSize(),
                                fontSize = fontBoxSize.sp,
                                color = Color(0xFF464646)
                            )
                        }
                    }
                }
            }
        return fragmentView
    }

    private fun subscribeObservers() {
        viewModel.dataState.observe(viewLifecycleOwner, Observer { dataState ->
            when (dataState) {
                is DataState.Success<BikeStrikeGo> -> {
                    displayProgressBar(false)
                    populateMainFragment(dataState.data)
                }
                is DataState.Error -> {
                    displayProgressBar(false)
                    displayError(dataState.exception.message)
                }
                is DataState.Loading -> {
                    displayProgressBar(true)
                }
            }
        })
    }

    private fun displayError(message: String?) {
        altitude.text = message
    }

    private fun populateMainFragment(data: BikeStrikeGo) {
        previousSpeed = speed.value
        speed.value = data.speed
        altitude.text = String.format("%.2f", data.altitude) + " m"
        distance.text = getAppropriateSpeedRepresentation(data.distance)
    }

    private fun getAppropriateSpeedRepresentation(distance: Double): String {
        return if(distance < 1000.0) {
            String.format("%.2f", distance) + " m"
        } else {
            String.format("%.2f", distance / 1000) + " km"
        }
    }

    private fun displayProgressBar(isDisplayed: Boolean) {
        progressbar.visibility = if (isDisplayed) View.VISIBLE else View.GONE
    }

    override fun onStart() {
        super.onStart()
        Log.d(TAG, "onStart: ")
        updateButtonState(
            sharedPreferences.getBoolean(SharedPreferenceUtil.KEY_FOREGROUND_ENABLED, false)
        )
        sharedPreferences.registerOnSharedPreferenceChangeListener(listener)
        val serviceIntent = Intent(activity, ForegroundLocationService::class.java)
        context!!.bindService(serviceIntent, foregroundServiceConnection, Context.BIND_AUTO_CREATE)
    }

    override fun onResume() {
        super.onResume()
        Log.d(TAG, "onResume: ")
        LocalBroadcastManager.getInstance(context!!).registerReceiver(
            foregroundBroadcastReceiver,
            IntentFilter(
                ForegroundLocationService.ACTION_FOREGROUND_LOCATION_BROADCAST
            )
        )
    }

    override fun onPause() {
        Log.d(TAG, "onPause: ")
        LocalBroadcastManager.getInstance(context!!).unregisterReceiver(
            foregroundBroadcastReceiver
        )
        super.onPause()
    }

    override fun onStop() {
        Log.d(TAG, "onStop: ")
        if (foregroundLocationServiceBound) {
            activity!!.unbindService(foregroundServiceConnection)
            foregroundLocationServiceBound = false
        }
        sharedPreferences.unregisterOnSharedPreferenceChangeListener(listener)
        super.onStop()
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d(TAG, "onDestroy: ")
    }

    private fun onSharedPreferenceChanged(sharedPreferences: SharedPreferences, key: String) {
        // Updates button states if new while in use location is added to SharedPreferences.
        if (key == SharedPreferenceUtil.KEY_FOREGROUND_ENABLED) {
            updateButtonState(
                sharedPreferences.getBoolean(SharedPreferenceUtil.KEY_FOREGROUND_ENABLED, false)
            )
        }
    }

    //Review Permissions: Method requests permissions.
    private fun requestForegroundPermissions() {
        provideRationale.launch(Manifest.permission.ACCESS_FINE_LOCATION)
    }

    //Review Permissions: Handles permission result.
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        Log.d(TAG, "onRequestPermissionResult")

        when (requestCode) {
            REQUEST_FOREGROUND_ONLY_PERMISSIONS_REQUEST_CODE ->
                when {
                    grantResults.isEmpty() ->
                        // If user interaction was interrupted, the permission request
                        // is cancelled and you receive empty arrays.
                        Log.d(TAG, "User interaction was cancelled.")
                    grantResults[0] == PackageManager.PERMISSION_GRANTED -> {
                        // Permission was granted.
                        foregroundLocationService?.subscribeToLocationUpdates()
                        resetChronometer()
                        startChronometer()
                    }

                    else -> {
                        // Permission denied.
                        updateButtonState(false)

                        Snackbar.make(
                            mainFragmentContainer,
                            R.string.permission_denied_explanation,
                            Snackbar.LENGTH_LONG
                        ).setAction(R.string.settings) {
                            // Build intent that displays the App settings screen.
                            val intent = Intent()
                            intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
                            val uri = Uri.fromParts(
                                "package",
                                BuildConfig.APPLICATION_ID,
                                null
                            )
                            intent.data = uri
                            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                            startActivity(intent)
                        }.show()
                    }
                }
        }
    }

    private fun fineLocationPermissionApproved(): Boolean {
        val context = context ?: return false
        return PackageManager.PERMISSION_GRANTED == checkSelfPermission(
            context,
            Manifest.permission.ACCESS_FINE_LOCATION
        )
    }

    private fun updateButtonState(trackingLocation: Boolean) {
        if (trackingLocation) {
            foregroundLocationButton.text = getString(R.string.stop_location_updates_button_text)
        } else {
            foregroundLocationButton.text = getString(R.string.start_location_updates_button_text)
        }
    }

    private fun startChronometer() {
        if (!isChronometerRunning.value) {
            timer.base = SystemClock.elapsedRealtime() - pauseOffset
            timer.start()
            isChronometerRunning.value = true
        }
    }

    private fun pauseChronometer() {
        if (isChronometerRunning.value) {
            timer.stop()
            pauseOffset = SystemClock.elapsedRealtime() - timer.base
            isChronometerRunning.value = false
        }
    }

    private fun resetChronometer() {
        timer.base = SystemClock.elapsedRealtime()
        pauseOffset = 0
        isChronometerRunning.value = false
    }

    /**
     * Receiver for location broadcasts from [ForegroundLocationService].
     */
    private inner class ForegroundBroadcastReceiver : BroadcastReceiver() {

        override fun onReceive(context: Context, intent: Intent) {
            val location = intent.getParcelableExtra<Location>(
                ForegroundLocationService.EXTRA_LOCATION
            )
            if (location != null) {
                viewModel.setStateEvent(MainStateEvent.GetStartEvent(location))
            }
        }
    }


}