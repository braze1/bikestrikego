package com.braze.bikestrikego.ui

import android.location.Location
import android.util.Log
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.*
import com.braze.bikestrikego.model.BikeStrikeGo
import com.braze.bikestrikego.repository.MainRepository
import com.braze.bikestrikego.util.DataState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
@ExperimentalCoroutinesApi
class MainViewModel
@Inject
constructor(
    private val mainRepository: MainRepository,
    private val savedStateHandle: SavedStateHandle
) : ViewModel() {

    private val _dataState: MutableLiveData<DataState<BikeStrikeGo>> = MutableLiveData()
    val dataState: LiveData<DataState<BikeStrikeGo>>
        get() = _dataState

    fun setStateEvent(mainStateEvent: MainStateEvent) {
        viewModelScope.launch {
            when (mainStateEvent) {
                is MainStateEvent.GetStartEvent -> {
                    mainRepository.getBSGOdata(mainStateEvent.location).onEach { dataState ->
                        _dataState.value = dataState
                    }.launchIn(viewModelScope)
                }
                is MainStateEvent.None -> {
                    // other state
                }
                is MainStateEvent.SetStopEvent -> {
                    mainRepository.stopBSGOdata().onEach { dataState ->
                        _dataState.value = dataState
                    }.launchIn(viewModelScope)
                }
                else -> {
                    //TODO: think about it
                }
            }
        }
    }
}


sealed class MainStateEvent {

    class GetStartEvent(val location: Location) : MainStateEvent()

    object SetStopEvent : MainStateEvent()

    object None : MainStateEvent()
}


















